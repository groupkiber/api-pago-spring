package cl.miguelramos.webpay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
public class WebpayApplication implements WebMvcConfigurer {

  public static void main(String[] args) {
    SpringApplication.run(WebpayApplication.class, args);
  }
  @Override
  public void addCorsMappings(CorsRegistry registry) {
    // Permite solicitudes CORS a todos los endpoints desde el origen especificado
    registry.addMapping("/**").allowedOrigins("http://localhost:5173");
  }
}
