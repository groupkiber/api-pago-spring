package cl.miguelramos.webpay.models;

import java.util.List;

public class CarritoRequestModel {
  private List<ItemCarrito> items;
  private double total;

  // Constructor, getters y setters


  public CarritoRequestModel() {
  }

  public CarritoRequestModel(List<ItemCarrito> items, double total) {
    this.items = items;
    this.total = total;
  }

  public List<ItemCarrito> getItems() {
    return items;
  }

  public void setItems(List<ItemCarrito> items) {
    this.items = items;
  }

  public double getTotal() {
    return total;
  }

  public void setTotal(double total) {
    this.total = total;
  }
}