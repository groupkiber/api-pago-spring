package cl.miguelramos.webpay.controllers;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;



@Controller
public class MyErrorController implements ErrorController {

  @RequestMapping("/error")
  public String handleError(HttpServletRequest request, Model model) {
    Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

    if (status != null) {
      int statusCode = Integer.parseInt(status.toString());
      // Aquí puedes añadir más lógica en función del código de error
      model.addAttribute("codigoError", statusCode);
      model.addAttribute("mensajeError", "Descripción del error"); // Personaliza este mensaje según necesites
    }
    return "error"; // El nombre de la vista de error
  }

  public String getErrorPath() {
    return "/error";
  }
}
