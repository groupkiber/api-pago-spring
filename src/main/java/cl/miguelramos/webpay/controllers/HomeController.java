package cl.miguelramos.webpay.controllers;

import cl.miguelramos.webpay.models.CrearTokenModel;
import cl.miguelramos.webpay.models.RequestModel;
import cl.miguelramos.webpay.models.RespuestaVerificacionModel;
import cl.miguelramos.webpay.utilidades.Constantes;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

@Controller
@RequestMapping("/")
public class HomeController {

  @GetMapping("")
  public String home() {
    return "home/home";
  }

  @GetMapping("/pagar")
  public String pagar(Model model) {

    RestTemplate restTemplate = new RestTemplate();
    // Encabezados
    HttpHeaders headers = new HttpHeaders();
    headers.set("Tbk-Api-Key-Id", Constantes.WEBPAY_CODIGO_COMERCIO);
    headers.set("Tbk-Api-Key-Secret", Constantes.WEBPAY_CODIGO_SECRETO);
    headers.setContentType(MediaType.APPLICATION_JSON);

    // Crear el Json
    RequestModel post = new RequestModel("ordenCompra12345678", "sesion1234557545", 10000, "http://localhost:8006/respuesta");

    HttpEntity<RequestModel> request = new HttpEntity<>(post, headers);
    //* Enviar la petición al servidor
    ResponseEntity<CrearTokenModel> response = restTemplate.postForEntity(Constantes.WEBPAY_URL, request, CrearTokenModel.class);
    // le pasamos a la vista el atributo
    model.addAttribute("response", response.getBody());
    return "home/pagar";
  }

  @GetMapping("/respuesta")
  public String respuesta(Model model, @RequestParam("token_ws") String token_ws)
  {
    RestTemplate restTemplate = new RestTemplate();
    HttpHeaders headers = new HttpHeaders();
    headers.set("Tbk-Api-Key-Id", Constantes.WEBPAY_CODIGO_COMERCIO);
    headers.set("Tbk-Api-Key-Secret", Constantes.WEBPAY_CODIGO_SECRETO);
    headers.setContentType(MediaType.APPLICATION_JSON);
    headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

    HttpEntity<String> entity = new HttpEntity<>(headers);

    ResponseEntity<RespuestaVerificacionModel> response = restTemplate.exchange(Constantes.WEBPAY_URL+"/"+token_ws, HttpMethod.PUT, entity, RespuestaVerificacionModel.class);

    RespuestaVerificacionModel respuesta = response.getBody();
    model.addAttribute("respuesta", respuesta);
    model.addAttribute("token_ws", token_ws);
    return "home/respuesta";
  }
}
