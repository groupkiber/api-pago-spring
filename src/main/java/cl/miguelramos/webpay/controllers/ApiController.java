package cl.miguelramos.webpay.controllers;

import cl.miguelramos.webpay.models.CarritoRequestModel;
import cl.miguelramos.webpay.models.CrearTokenModel;
import cl.miguelramos.webpay.models.RequestModel;
import cl.miguelramos.webpay.models.RespuestaVerificacionModel;
import cl.miguelramos.webpay.utilidades.Constantes;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

@RestController
@RequestMapping("/api")
public class ApiController {

  @PostMapping("/pagar") // Cambiar de GetMapping a PostMapping
  public ResponseEntity<CrearTokenModel> pagar(@RequestBody CarritoRequestModel carritoRequest) {
    RestTemplate restTemplate = new RestTemplate();
    HttpHeaders headers = new HttpHeaders();
    headers.set("Tbk-Api-Key-Id", Constantes.WEBPAY_CODIGO_COMERCIO);
    headers.set("Tbk-Api-Key-Secret", Constantes.WEBPAY_CODIGO_SECRETO);
    headers.setContentType(MediaType.APPLICATION_JSON);

    // Aquí, utiliza los datos de carritoRequest para crear tu RequestModel
    RequestModel post = new RequestModel(
            // Aquí debes generar o utilizar los datos de la orden de compra y sesión adecuadamente
            "ordenCompra12345678",
            "sesion1234557545",
            (int) carritoRequest.getTotal(),
            "http://localhost:5173/respuesta"
    );

    HttpEntity<RequestModel> request = new HttpEntity<>(post, headers);
    ResponseEntity<CrearTokenModel> response = restTemplate.postForEntity(Constantes.WEBPAY_URL, request, CrearTokenModel.class);
    return ResponseEntity.ok(response.getBody());
  }
  @GetMapping("/respuesta")
  public ResponseEntity<RespuestaVerificacionModel> respuesta(@RequestParam("token_ws") String token_ws) {
    RestTemplate restTemplate = new RestTemplate();
    HttpHeaders headers = new HttpHeaders();
    headers.set("Tbk-Api-Key-Id", Constantes.WEBPAY_CODIGO_COMERCIO);
    headers.set("Tbk-Api-Key-Secret", Constantes.WEBPAY_CODIGO_SECRETO);
    headers.setContentType(MediaType.APPLICATION_JSON);
    headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

    HttpEntity<String> entity = new HttpEntity<>(headers);
    ResponseEntity<RespuestaVerificacionModel> response = restTemplate.exchange(Constantes.WEBPAY_URL+"/"+token_ws, HttpMethod.PUT, entity, RespuestaVerificacionModel.class);

    return ResponseEntity.ok(response.getBody());
  }
}
