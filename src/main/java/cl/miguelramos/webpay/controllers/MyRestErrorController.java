package cl.miguelramos.webpay.controllers;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class MyRestErrorController implements ErrorController {

  @RequestMapping("/error")
  public ResponseEntity<?> handleError(HttpServletRequest request) {
    Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
    HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
    String message = "Descripción del error"; // Personaliza este mensaje según necesites

    if (status != null) {
      int statusCode = Integer.parseInt(status.toString());
      httpStatus = HttpStatus.valueOf(statusCode);
      // Aquí puedes añadir más lógica en función del código de error
      // y personalizar el mensaje de error según el statusCode
    }

    return ResponseEntity.status(httpStatus)
            .body(new ErrorResponse(httpStatus.value(), message));
  }

  public String getErrorPath() {
    return "/error";
  }

  // Clase interna para respuestas de error
  static class ErrorResponse {
    private int codigoError;
    private String mensajeError;

    ErrorResponse(int codigoError, String mensajeError) {
      this.codigoError = codigoError;
      this.mensajeError = mensajeError;
    }

    // Getters y setters...
  }
}
